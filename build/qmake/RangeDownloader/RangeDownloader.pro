QT -= gui
QT += network core
CONFIG += c++14 console
CONFIG -= app_bundle
DESTDIR = build
#put temp files in hidden folders
OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.ui

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += ../../../src/main.cpp \
    ../../../src/NetworkRequest.cpp

HEADERS += \
    ../../../inc/NetworkRequest.h \
    ../../../inc/CommonInclude.h

INCLUDEPATH += ../../../inc/
