#pragma once
#include <memory>
#include <QUrl>
#include <QtNetwork>
namespace RangeDownloader {
    class NetworkRequest
    {
    public:
        NetworkRequest();
        //Public api to request a file from a url
        static std::vector<std::unique_ptr<QNetworkReply>> requestFile(const QUrl & requestUrl,
                                                                unsigned int totalBlocks,
                                                                unsigned int blockSize,
                                                                unsigned int totalBytesToRequest,
                                                                QNetworkAccessManager &accessManager);
    private:
        //Helper function to create the http header
        static std::unique_ptr<QNetworkReply> createRequests(const QUrl & downloadUrl,
                                                             const std::string & range,
                                                             QNetworkAccessManager &accessManager,
                                                             unsigned int blockSize);

    };
}
