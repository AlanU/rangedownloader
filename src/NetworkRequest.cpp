#include "NetworkRequest.h"
using namespace RangeDownloader;
NetworkRequest::NetworkRequest()
{

}

//std::move may cause a compiler warning on some compilers, since a Copy Elision optimzation could avoid the move and the attempts to try to copy
//std::move guarantees the move, this is to avoid any attempts to try to copy the std::unique_ptr (which can not be copied)
//In c++ 11 and 14 Copy Elision is not guaranteed. If the compiler supprorts Copy Elision std::move can be removed
//C++ 17 guaranties Copy Elision
//accessManager can't be const reference since its get method is not const
std::unique_ptr<QNetworkReply> NetworkRequest::createRequests(const QUrl & downloadUrl,
                                           const std::string & range,
                                           QNetworkAccessManager &accessManager,
                                           unsigned int blockSize)
{
    QNetworkRequest downloadRequest(downloadUrl);
    downloadRequest.setRawHeader(QByteArray("Range"),QByteArray(range.c_str()));
    std::unique_ptr<QNetworkReply> reply (accessManager.get(downloadRequest));
    reply->setReadBufferSize(blockSize);

    return std::move(reply);
}

//Loops totalBlocks times creating the block range headers and request
std::vector<std::unique_ptr<QNetworkReply>> NetworkRequest::requestFile(const QUrl & requestUrl,
                                                                        unsigned int totalBlocks,
                                                                        unsigned int blockSize,
                                                                        unsigned int totalBytesToRequest,
                                                                        QNetworkAccessManager &accessManager)
{
    unsigned int startBytes = 0;
    unsigned int endBytes = 0;
    std::vector<std::unique_ptr<QNetworkReply>>replyVector;
    replyVector.reserve(totalBlocks);
    //-1 is do to the fact the bytes are zero index
    for (unsigned int index = 0; index < totalBlocks; ++index)
    {
        if(startBytes + blockSize > totalBytesToRequest)//left over bytes that do not fit into a whole block
        {
            endBytes = totalBytesToRequest - 1;
        }
        else
        {
            endBytes = startBytes + blockSize -1 ;
        }
        std::string rangeString = "bytes="+std::to_string(startBytes)+"-"+std::to_string(endBytes);
        replyVector.push_back(createRequests(requestUrl,rangeString,accessManager,blockSize));
        startBytes = endBytes+1; //we plus one the last end value to start at the next byte since endbytes includes a byte in the last range
    }
    return std::move(replyVector);
}
