#pragma once
#include <string>
#include <QUrl>
#include <QCommandLineParser>
#include <iostream>
namespace RangeDownloader {
    //these need to be const char * since QString does not have an constructor for std::string
    constexpr const char * sourceCommand = "source-url";
    constexpr const char * amountSizeCommand = "amount-size";
    constexpr const char * chunkSizeCommand = "chunck-size";
    constexpr const char * chunkNumberCommand = "chunck-number";
    constexpr const char * outputFileCommand = "outputfile";
    constexpr int downloadUrlPramIndex = 0;
    constexpr int amountSizePramIndex = 1;

    enum class ReturnCode: int
    {
        unknown = 1,
        downloadError,
        createRequestsError,
        argumentParseError,
        invaildUrlError,
        invalidFileOpenError,
        invalidFileWriteError,
        invalidChunckNumber,
        noError
    };

}
