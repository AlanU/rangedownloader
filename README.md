Usage: ./RangeDownloader [options] source-url amount-size

To downlaod part of a file using get network request

Options:

  -v, --version             Displays version information.

  -h, --help                Displays this help.

  --chunck-size `<bytes>`     Size of download chunck in bytes

  --chunck-number `<number>`  Number of download chunck. If this is set chunck-size is ignored

  --outputfile `<path>`       The path with file name to write the output If not set it is written to working dir as the file output

Arguments:

  source-url                Source url to download

  amount-size               Amount of file to download in bytes
  
  Example 
  
./RangeDownloader `<url>` 4000000 --chunck-size=1000000

./RangeDownloader `<url>` 4000000 --chunck-number=4