#include <QCoreApplication>
#include <QObject>
#include <QUrl>
#include <QFile>
#include <QString>
#include <memory>
#include <math.h>
#include <vector>
#include <string>
#include <iostream>
#include <chrono>
#include <thread>

#include "NetworkRequest.h"
#include "CommonInclude.h"
using namespace RangeDownloader;

//Prints out error and returns the correct return code number for the exiting of the program
int errorWithReturnCode(ReturnCode code,const std::string & errorString = "")
{
    switch (code) {
    case ReturnCode::downloadError:
            std::cout <<"The was an error downloading"<<errorString<<std::endl;
        break;
    case ReturnCode::createRequestsError:
            std::cout <<"error in constructing request"<<errorString<<std::endl;
        break;
    case ReturnCode::invaildUrlError:
            std::cout<<"Invalid Url"<<errorString<<std::endl;
         break;
    case ReturnCode::invalidChunckNumber:
            std::cout<<"Total bytes can not be evenly devided by Chunck Number"<<" use "<<chunkSizeCommand<<" instead"<<std::endl;
        break;
    case ReturnCode::invalidFileOpenError:
    case ReturnCode::argumentParseError:
            std::cout<<errorString<<std::endl;
         break;
    default:
           std::cout<<"unknown error"<<errorString<<std::endl;
        break;
    }
    return static_cast<int>(code);
}


//Parse Command line arguments and set downloadUrl totalbytes chunckNumber outputFile correctly
//Also validates the data is with in correct ranges
int parseArguments(const QCommandLineParser & parser,
                   QUrl & downloadUrl,
                   unsigned int & totalbytes,
                   unsigned int & chunckNumber,
                   unsigned int & chunckSize,
                   std::string & outputFile)
{
    const QStringList pargs = parser.positionalArguments();
    if(pargs.size() < 2) // requires 2 arguments
    {
        return errorWithReturnCode(ReturnCode::argumentParseError,std::string("Requires ") +
                                            std::string(sourceCommand) +
                                            std::string(" and ") +
                                            std::string(amountSizeCommand));
    }

    downloadUrl = QUrl::fromUserInput(pargs[downloadUrlPramIndex]);
    if(!downloadUrl.isValid())
    {
        return errorWithReturnCode(ReturnCode::invaildUrlError);
    }

    totalbytes = pargs[amountSizePramIndex].toUInt();
    if(totalbytes == 0)
    {
        return errorWithReturnCode(ReturnCode::argumentParseError,std::string(amountSizeCommand)+" must be larger then zero");
    }

    if (parser.isSet(chunkNumberCommand))
    {
        chunckNumber = parser.value(chunkNumberCommand).toUInt();
        if(chunckNumber == 0)
        {
            return errorWithReturnCode(ReturnCode::argumentParseError,std::string(chunkNumberCommand)+" must be larger then zero");
        }
    }

    if(parser.isSet(chunkSizeCommand))
    {
       chunckSize = parser.value(chunkSizeCommand).toUInt();
       if(chunckSize == 0)
       {
           return errorWithReturnCode(ReturnCode::argumentParseError,std::string(chunkSizeCommand)+" must be larger then zero");
       }
    }

    if(parser.isSet(outputFileCommand))
    {
        outputFile = parser.value(outputFileCommand).toStdString();
    }

    return static_cast<int>(ReturnCode::noError);
}

int main(int argc, char *argv[])
{
    QCoreApplication app (argc, argv);
    QCoreApplication::setApplicationName("RangeDownloader");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    QUrl downloadUrl;
    unsigned int totalBytes = 0;
    unsigned int chunckSize = 1;
    unsigned int chunckNumber = 0;
    std::string outputFile = "output";

    //Set up command line
    parser.setApplicationDescription(QObject::tr("To downlaod part of a file using get network request"));
    parser.addVersionOption();
    parser.addHelpOption();
    parser.addPositionalArgument(sourceCommand,QObject::tr("Source url to download"));
    parser.addPositionalArgument(amountSizeCommand,QObject::tr("Amount of file to download in bytes"));
    parser.addOptions({
                      {chunkSizeCommand,QObject::tr("Size of download chunck in bytes"),QObject::tr("bytes")},
                      {chunkNumberCommand,QObject::tr("Number of download chunck. If this is set ")+
                       chunkSizeCommand+QObject::tr(" is ignored"),QObject::tr("number")},
                      {outputFileCommand,QObject::tr("The path with file name to write the output\n If not set it is written to working dir as the file output"),
                           QObject::tr("path")},
                      });

    parser.process(app);

    int errorCode = parseArguments(parser,downloadUrl,totalBytes,chunckNumber,chunckSize,outputFile);
    //check parsing arguments did not return and error
    if(errorCode != static_cast<int>(ReturnCode::noError))
    {
       //show help will exit the program with errorcode no need to call return
        parser.showHelp(errorCode);
    }


    QNetworkAccessManager accessManager;
    //calculate totalChuncks chunckSize totalChuncks
    unsigned int totalChuncks = 1;
    if(chunckNumber != 0)//If chuck number is specify calcaualte chunckSize
    {
        totalChuncks = chunckNumber;
        if((totalBytes %  totalChuncks) == 0 )
        {
            chunckSize =  static_cast<float>(totalBytes)/static_cast<float>(totalChuncks);
        }
        else
        {
            //error if total bytes can not be downloaded with total chuncks evenly
            parser.showHelp(errorWithReturnCode(ReturnCode::invalidChunckNumber));
        }


    }
    else // If chuck number is not specify calculate number of chuncks
    {
        totalChuncks = ceil(static_cast<float>(totalBytes)/static_cast<float>(chunckSize));
    }

    //Make request for file and retuns a vector of unique_ptr QNetworkReply
    //The vector is return by move not copy
    std::vector<std::unique_ptr<QNetworkReply>>replys = NetworkRequest::requestFile(downloadUrl,totalChuncks,chunckSize,totalBytes,accessManager) ;

    unsigned int replyWatingFor = 0;
    int_fast64_t totalBytesRead = 0;
    //Using QFile over iostream since its simplier to write out the byte array and can delete any incomplete file incase of error
    QFile outFile(outputFile.c_str());
    if(!outFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        return errorWithReturnCode(ReturnCode::invalidFileOpenError,std::string("Could not open ")+outputFile);
    }
    //Loop and check to see if responses is finished
    std::cout<<"Waiting for Response"<<std::endl;
    do
    {
       if(replys[replyWatingFor] == nullptr)
       {
           return errorWithReturnCode(ReturnCode::createRequestsError);
       }
       if(replys[replyWatingFor]->isFinished())//isFinished returns true if success or on abort
       {
           if(replys[replyWatingFor]->error() == QNetworkReply::NoError)
           {
               QByteArray response_data = replys[replyWatingFor]->readAll();//copies data of reply localy for writting to guarantees it will exist in scope
               int bytesRead = response_data.size();
               totalBytesRead += bytesRead; // recode the total bytes read
               int_fast64_t bytesWritten = outFile.write(response_data);

               //check for error in total bytes written if they are not the same as the bytes read there is a problem
               if(bytesWritten != bytesRead)
               {
                   outFile.remove();
                   outFile.close();
                   return errorWithReturnCode(ReturnCode::invalidFileWriteError,std::string("Error Writting out file: ")+
                                                                                outFile.errorString().toStdString());

               }

               replys[replyWatingFor] = nullptr;
               ++replyWatingFor;
               //write out status of download
               float percentDone= static_cast<float>(replyWatingFor)/static_cast<float>(totalChuncks) * 100;
               std::cout<<percentDone<<"% Done Total Bytes Read "<<totalBytesRead<<std::endl;
           }
           else
           {
                 outFile.remove();
                 outFile.close();
                 return errorWithReturnCode(ReturnCode::downloadError);
           }

      }
      //pump the Qt event loop
      app.processEvents();
      //sleep 66 to give a ~15 FPS main loop to cut down on cpu usage so the program does peg the users cpu
      std::this_thread::sleep_for(std::chrono::milliseconds(66));
    }while(replyWatingFor != totalChuncks);
    outFile.close();
    return 0;
}
